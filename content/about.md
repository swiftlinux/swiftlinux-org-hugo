---
title: About
---

### Parent Distros
Swift Linux is based on <a href="https://mxlinux.org/">MX Linux</a>, MX Linux is based on <a href="http://antix.mepis.org">antiX Linux</a>, and antiX Linux is based on the stable branch of <a href="https://www.debian.org/">Debian Linux</a>.

### Source Code
The source code is on <a href="https://gitlab.com/swiftlinux">GitLab</a>.

### What does Swift Linux add to MX Linux?
Every edition of Swift Linux adds updates, KeePassXC, and special themed wallpaper for the login screen and desktop.  Yes, every edition of Swift Linux is a special edition.

### Did you invent Hannah Montana Linux?
No, that wasn't me.  However, I liked the idea.  Because the person who invented Hannah Montana Linux abandoned the project, I decided to revive it.

### Why did you pick MX Linux as the basis of Swift Linux?
* MX Linux is my favorite Linux distro, because it is so lightweight, fast, and user-friendly.
* MX Linux has a large user base.
* The Debian base of MX Linux is lighter, more stable, and lower in maintenance needs than Ubuntu Linux.
* MX Linux has a great team with lots of experience.  It's the same team that first released antiX Linux in 2007 and MEPIS Linux in 2003.
* In other words, MX Linux is the Linux Mint of lightweight distros.  In fact, I regard MX Linux as the unofficial Linux Mint Debian Edition, because it offers many of the same merits of Linux Mint in a lighter and faster package.

### Is there a regular edition of Swift Linux?
No, there is not.  EVERY edition of Swift Linux is a special edition.

### Why isn't Swift Linux based directly on Debian?
* This would require me to create a Swift Linux repository with packages transforming the base Debian setup into Swift Linux.  These packages would provide the user interface, driver support, software libraries, and numerous other details needed to create a polished and user-friendly distro.  I don't have the knowledge and capability needed to do all this.  The MX Linux team has already done an excellent job for years.  In fact, MX Linux is so great that it's hard to think of ways to further improve it.
* MX Linux and the full version of antiX Linux now include preinstalled LibreOffice, and so do nearly all other general purpose Linux distros.
* Keeping the ISO file small enough to fit onto a CD was once a worthy goal but is no longer relevant, because even 10-year-old computers can handle DVDs, use of optical media is on the decline, and booting up from USB drives (which are faster and have plenty of capacity for any Linux distro) is much more common.
